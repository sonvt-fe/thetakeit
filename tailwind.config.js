/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/pages/**/*.{js,ts,jsx,tsx}', './src/components/**/*.{js,ts,jsx,tsx}', './src/app/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      screens: {
        xs: '375px',
      },
      fontSize: {
        xs: '0.75rem',
      },
      padding: {
        '2/5': '40%',
        '3/4': '75%',
        full: '100%',
      },
      colors: {
        primary: '#e36c0a',
        facebook: '#365899',
        twitter: '#00acee',
        linkedin: '#0072b1',
        youtube: '#ff0000',
      },
      typography: {
        DEFAULT: {
          css: {
            color: '#000',
            a: {
              color: '#2c5282',
              '&:hover': {
                color: '#e36c0a',
              },
            },
          },
        },
      },
    },
  },
  plugins: [require('@tailwindcss/typography')],
};
