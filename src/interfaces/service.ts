import { StaticImageData } from 'next/image';

export interface IService {
  id: string;
  title: string;
  uri: string;
  photo: StaticImageData;
  desc: string;
  detail: string;
  createAt: string;
}
