import othersServicePhoto from '@/public/images/services/other-services.png';
import tradingConsultancyPhoto from '@/public/images/services/trading-consultancy.png';
import travelAgencyPhoto from '@/public/images/services/travel-agency.png';

const data = {
  en: [
    {
      id: '54af76d9-8685-4c7d-84de-999d781ceb3a',
      title: 'Trading consultancy',
      uri: 'trading-consultancy',
      photo: tradingConsultancyPhoto,
      desc: 'We provide the services on connecting pharmaceutical manufacturers all around the world to market and sell their products in Viet nam and vice versa',
      detail:
        '<p>As a professional in Vietnamese market for more than 20 years and in Canada market for more than 8 years, the company would provide the consulting services in these fields:</p><p><b>Pharmaceutical: we are authorised representative for</b></p><ul><li>We offer the consultancy services in M&A activities for pharmaceutical manufacturers in Viet Nam</li><li>Our services is to help your products to be registered in local Ministry of Health and can be marketed in local country</li></ul><p>Reference info:</p><p><b>Viet Nam as a quick glance:</b></p><ul><li>CAPITAL: Hanoi.</li><li>POPULATION: 97,040,334.</li><li>OFFICIAL LANGUAGE: Vietnamese.</li><li>MONEY: Dong.</li><li>AREA: 127,123 square miles (329,247 square kilometers)</li></ul><p><b>Pharmaceutical market in Viet Nam:</b></p><p>Vietnam pharmaceuticals market is anticipated to grow at a formidable rate during the forecast period. The market growth can be attributed to the rising development of medicinal drugs and pharmaceutical products and the growing healthcare infrastructure in the country.</p><p>Vietnam Pharmaceuticals Market, By Drug Type:</p><ul><li>Generic Drugs</li><li>Branded Drugs</li></ul><p>Vietnam Pharmaceuticals Market, By Product Type:</p><ul><li>Prescription Drugs</li><li>Over-The-Counter Drugs</li></ul><p>Vietnam Pharmaceuticals Market, By Application:</p><ul><li>Cardiovascular</li><li>Musculoskeletal</li><li>Oncology</li><li>Anti-infective</li><li>Metabolic Disorder</li><li>Others</li></ul><p>Vietnam Pharmaceuticals Market, By Distribution Channel:</p><ul><li>Retail Pharmacy</li><li>Hospital Pharmacy</li><li>E-Pharmacy</li></ul><p>Vietnam Pharmaceuticals Market, By Region:</p><ul><li>Northern</li><li>Central</li><li>Southern</li></ul><p>Currently, a lot of local manufacturers in Viet Nam already have gotten the EU GMP and looking forward to export their products to other markets around the world. They also look for a joint venture or a buyer to take over their business.</p><p><a href="https://moh.gov.vn/en_US/web/ministry-of-health" target="_blank" rel="noopener noreferrer">Portal of the Ministry of Health (moh.gov.vn)</a></p><p>Food and beverages: we are authorised representative for<a href="https://584nhatrang.vn/" target="_blank" rel="noopener noreferrer">https://584nhatrang.vn/</a></p>',
      createAt: '2023-04-23T09:12:46.268Z',
    },
    {
      id: 'fc7e1574-e74a-4888-b5b4-11f58f77a7cc',
      title: 'Travel agency',
      uri: 'travel-agency',
      photo: travelAgencyPhoto,
      desc: 'We are the branch of Royal tourist in Viet Nam and US. We provide airticket booking for local Vietnamese Canadian with the best price and best routes. In addition, as an authorised agent, we are providing the all-in tour',
      detail: '<center><a href="https://www.facebook.com/Hoanggiatouristpage" target="_blank" rel="noopener noreferrer"><img src="/images/services/ve-may-bay-hoang-gia.jpg" alt="Vé máy bay Hoàng Gia" loading="lazy"></a></center><center><a href="http://royaltourist.com.vn/" target="_blank" rel="noopener noreferrer"><img src="/images/services/wear-red-day.png" alt="Royal tourist" loading="lazy"></a></center>',
      createAt: '2023-04-23T09:12:46.268Z',
    },
    {
      id: 'ba520ed7-b76c-457e-8a87-f76d13188da1',
      title: 'Other services',
      uri: 'other-services',
      photo: othersServicePhoto,
      desc: 'We take care of financing for corporate and individuals for those living in Canada',
      detail:
        '<p><b>Insurance</b></p><p>For individuals, we provide the travel and health insurance services for traveler and commercial insurance: car, house insurance.</p><p><b>Finance</b></p><p>For corporate, we offer payroll services which include:</p><ul><li>Monthly remittances and payroll calculations for staff</li><li>At the end of the year we also do T4’s, T5’s, T4A’s and T5018’s for your staff or contractors</li><li>For individuals, we can obtain any information that CRA provides through their downloads</li><li>scanning of all your documents and do Efiling of tax returns</li></ul>',

      createAt: '2023-04-23T09:12:46.268Z',
    },
  ],
  vi: [
    {
      id: '54af76d9-8685-4c7d-84de-999d781ceb3a',
      title: 'Tư vấn, hỗ trợ, hợp tác kinh doanh, mua bán doanh nghiêp',
      uri: 'trading-consultancy',
      photo: tradingConsultancyPhoto,
      desc: 'Chúng tôi cung cấp các dịch vụ môi giới nhằm kết nối các nhà sản xuất dược và thực phẩm trên toàn thế giới để bán hàng tại thị trường Việt Nam và các chiều ngược lại',
      detail:
        '<p>Là một chuyên gia tại thị trường Việt Nam hơn 20 năm và tại thị trường Canada hơn 8 năm, công ty sẽ cung cấp các dịch vụ tư vấn trong các lĩnh vực:</p><p><b>Dược phẩm: chúng tôi là đại diện ủy quyền cho</b></p><p>Dịch vụ của chúng tôi là giúp sản phẩm của bạn được đăng ký tại Bộ Y tế địa phương và có thể được bán trên thị trường địa phương quốc gia</p><p>Tư vấn mua bán công ty sản xuất dược phẩm tại Việt Nam:</p><p><b>Tổng quan về Việt Nam:</b></p><ul><li>VỐN: Hà Nội.</li><li>DÂN SỐ: 97.040.334.</li><li>NGÔN NGỮ CHÍNH THỨC: Tiếng Việt.</li><li>TIỀN: Đồng.</li><li>DIỆN TÍCH: 127.123 dặm vuông (329.247 km vuông)</li></ul><p><b>Thị trường dược phẩm tại Việt Nam:</b></p><p>Thị trường dược phẩm Việt Nam được dự đoán sẽ tăng trưởng với tốc độ chóng mặt trong giai đoạn dự báo. Thị trường tăng trưởng có thể là do sự phát triển ngày càng tăng của các loại thuốc và dược phẩm và sự gia tăng cơ sở hạ tầng y tế trong nước.</p><p>Thị Trường Dược Việt Nam, Theo Loại Thuốc:</p><ul><li>Thuốc gốc</li><li>Thuốc có nhãn hiệu</li></ul><p>Thị trường dược phẩm Việt Nam, theo loại sản phẩm:</p><ul><li>Thuốc theo toa</li><li>Vượt quá số dược liệu</li></ul><p>Thị Trường Dược Phẩm Việt Nam, Theo Ứng Dụng:</p><ul><li>Tim mạch</li><li>Cơ xương khớp</li><li>Ung thư</li><li>Chống nhiễm trùng</li><li>Rối loạn trao đổi chất</li><li>Khác</li></ul><p>Thị Trường Dược Việt Nam, Theo Kênh Phân Phối:</p><ul><li>Nhà thuốc bán lẻ</li><li>Nhà thuốc bệnh viện</li><li>hiệu thuốc điện tử</li></ul><p>Thị trường dược phẩm Việt Nam, theo khu vực:</p><ul><li>Phương bắc</li><li>Trung tâm</li><li>Phía Nam</li></ul><p>Hiện nay, nhiều nhà sản xuất trong nước tại Việt Nam đã đạt tiêu chuẩn GMP của EU và mong muốn xuất khẩu sản phẩm sang các thị trường khác trên thế giới. Họ cũng tìm kiếm một liên doanh hoặc người mua để tiếp quản công việc kinh doanh của họ.</p><p><a href="https://moh.gov.vn/en_US/web/ministry-of-health" target="_blank" rel="noopener noreferrer">Cổng thông tin của Bộ Y tế (moh.gov.vn)</a></p><p>Thực phẩm và đồ uống: chúng tôi là đại diện được ủy quyền cho&nbsp;<a href="https://584nhatrang.vn/" target="_blank" rel="noopener noreferrer">https://584nhatrang.vn/</a></p>',
      createAt: '2023-04-23T09:12:46.268Z',
    },
    {
      id: 'fc7e1574-e74a-4888-b5b4-11f58f77a7cc',
      title: 'Đại lý du lịch, vé máy bay',
      uri: 'travel-agency',
      photo: travelAgencyPhoto,
      desc: 'Chúng tôi là đại diện của công ty du lịch Hoàng Gia nhằm cung cấp vé máy bay với giá rẻ hoặc đường bay tốt nhất cho các kiều bào tại Canada , đồng thời là nhà tổ chức du lịch chuyên nghiêp với các tour du lịch quốc tế trọn gói giá tốt nhất phù hợp tài chính của bạn',
      detail: '<center><a href="https://www.facebook.com/Hoanggiatouristpage" target="_blank" rel="noopener noreferrer"><img src="/images/services/ve-may-bay-hoang-gia.jpg" alt="Vé máy bay Hoàng Gia" loading="lazy"></a></center><center><a href="http://royaltourist.com.vn/" target="_blank" rel="noopener noreferrer"><img src="/images/services/wear-red-day.png" alt="Royal tourist" loading="lazy"></a></center>',
      createAt: '2023-04-23T09:12:46.268Z',
    },
    {
      id: 'ba520ed7-b76c-457e-8a87-f76d13188da1',
      title: 'Dịch vụ khác',
      uri: 'other-services',
      photo: othersServicePhoto,
      desc: 'Các bạn hãy tập trung vào chuyên môn của mình, hãy để công ty chúng tôi chăm lo tài chính cho công ty hoặc cá nhân bạn',
      detail:
        '<p><b>Bảo hiểm</b></p><p>Đối với cá nhân, chúng tôi cung cấp dịch vụ bảo hiểm du lịch và bảo hiểm y tế cho khách du lịch và bảo hiểm thương mại: bảo hiểm xe hơi, nhà cửa.</p><p><b>Tài chính</b></p><p>Đối với doanh nghiệp, chúng tôi cung cấp dịch vụ tính lương bao gồm:</p><ul><li>Chuyển tiền hàng tháng và tính lương cho nhân viên</li><li>Vào cuối năm, chúng tôi cũng làm T4, T5, T4A và T5018 cho nhân viên hoặc nhà thầu của bạn</li><li>quét tất cả các tài liệu của bạn và thực hiện Khai thuế</li></ul><p>Đối với các cá nhân, chúng tôi có thể lấy bất kỳ thông tin nào mà CRA cung cấp tải xuống và thực hiện khai thuế hằng năm.</p>',
      createAt: '2023-04-23T09:12:46.268Z',
    },
  ],
};

export default data;
