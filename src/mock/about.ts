const data = {
  en: {
    title: 'Tracy Duong, CPA, ACCA, Master of Finance',
    desc: 'Tracy brings a wealth of experience of more than 20 years in accounting, financial reporting, audit and tax in PwC, Hewlett-Packard and other MNCs. She is a Chartered Professional Accountant (CPA, CGA) who earned her designation in 2005 as ACCA – Association of Chartered Certified Accountants – United Kingdom.',
    detail:
      '<p>Founded in 2020, The Take It in is a Toronto service providing firm which serves a variety of small and medium-sized businesses, individuals, across Canada and Viet Nam.</p><p>Key Factors That Enabled Us To Be A Favorite Service providing Firm</p><p>The following is a listing of key factors that allowed The Take It Inc. to be a favorite firm in Toronto, Canada.</p><p>As a Canadian and Viet Namese CPA, The Take It Inc. offers full-scale services to clients in a large array of industries. It feels it can best meet clients’ needs by addressing the specific problems that affect each industry.</p><p>We connect Viet Nam and Canada market. Here’s an overview of just a few of the specialized industry sectors:</p><ul><li>Pharmaceutical</li><li>Retail: Food & Beverage</li><li>Travel agency</li><li>Finance & Insurance</li><li>Professional Service Firms</li></ul>',
    btnLabel: 'View more',
    link: '/en/about',
  },
  vi: {
    title: 'Tracy Duong, CPA, ACCA, Thạc sĩ Tài chính',
    desc: 'Tracy có bề dày kinh nghiệm hơn 20 năm về kế toán, báo cáo tài chính, kiểm toán và thuế tại PwC, Hewlett-Packard và các MNC khác. Cô là Kế toán viên Chuyên nghiệp Công chứng (CPA, CGA), người đã được chỉ định vào năm 2005 với tư cách là ACCA – Hiệp hội Kế toán Công chứng Công chứng – Vương quốc Anh.',
    detail:
      '<p>Được thành lập vào năm 2020, The Take It in là một công ty cung cấp dịch vụ ở Toronto, phục vụ nhiều cá nhân, doanh nghiệp vừa và nhỏ trên khắp Canada và Việt Nam.</p><p>Các yếu tố chính giúp chúng tôi trở thành công ty cung cấp dịch vụ được yêu thích</p><p>Sau đây là danh sách các yếu tố chính giúp The Take It Inc. trở thành công ty được yêu thích ở Toronto, Canada.</p><p>Là CPA của Canada và Việt Nam, The Take It Inc. cung cấp các dịch vụ toàn diện cho khách hàng trong nhiều ngành công nghiệp. Nó cảm thấy nó có thể đáp ứng tốt nhất nhu cầu của khách hàng bằng cách giải quyết các vấn đề cụ thể ảnh hưởng đến từng ngành.</p><p>Chúng tôi kết nối thị trường Việt Nam và Canada. Dưới đây là tổng quan về một số lĩnh vực công nghiệp chuyên biệt:</p><ul><li>Dược phẩm</li><li>Bán lẻ: Thực phẩm & Đồ uống</li><li>Đại lý du lịch</li><li>Tài chính & Bảo hiểm</li><li>Công ty dịch vụ chuyên nghiệp</li></ul>',
    btnLabel: 'Xem thêm',
    link: '/vi/about',
  },
};

export default data;
