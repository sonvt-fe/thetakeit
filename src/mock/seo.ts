const data = {
  home: {
    en: {
      title: 'The take it',
      description:
        'Founded in 2020, The Take It in is a Toronto service providing firm which serves a variety of small and medium-sized businesses, individuals, across Canada and Viet Nam.',
    },
    vi: {
      title: 'The take it',
      description:
        'Được thành lập vào năm 2020, The Take It in là một công ty cung cấp dịch vụ ở Toronto, phục vụ nhiều cá nhân, doanh nghiệp vừa và nhỏ trên khắp Canada và Việt Nam.',
    },
  },
  about: {
    en: {
      title: 'About',
      description:
        'Tracy is a Chartered Professional Accountant (CPA, CGA) who earned her designation in 2005 as ACCA – Association of Chartered Certified Accountants – United Kingdom.',
    },
    vi: {
      title: 'Giới thiệu',
      description:
        'Tracy là Kế toán viên Chuyên nghiệp Công chứng (CPA, CGA), người đã được chỉ định vào năm 2005 với tư cách là ACCA – Hiệp hội Kế toán Công chứng Công chứng – Vương quốc Anh.',
    },
  },
  service: {
    en: {
      title: 'Services',
      description:
        'Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs.',
    },
    vi: {
      title: 'Dịch vụ',
      description:
        'Tận dụng lợi thế thấp để xác định hoạt động giá trị gia tăng sân bóng để thử nghiệm beta. Ghi đè khoảng cách kỹ thuật số bằng các lần nhấp bổ sung.',
    },
  },
  contact: {
    en: {
      title: 'Contact',
    },
    vi: {
      title: 'Liên hệ',
    },
  },
};

export default data;
