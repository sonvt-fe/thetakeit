import { Locale } from '@/config/i18n-config';

export const isGGPageSpeedDevice = () => {
  const e = navigator.userAgent;
  return /google page speed insights/i.test(e) || /chrome-lighthouse/i.test(e);
};

export const formatDate = (dateString: string, locale: Locale): string => {
  const date = new Date(dateString);
  return date.toLocaleDateString(locale === 'en' ? 'en-US' : 'vi-VN', { hour: 'numeric', minute: 'numeric' });
};
