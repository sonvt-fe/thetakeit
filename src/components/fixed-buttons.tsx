'use client'
import { Locale } from '@/config/i18n-config';
import { EMAIL, PHONE_NUMBER, SOCIALS } from '@/constants/common';
import Link from 'next/link';
import React from 'react';
import { FaEnvelope, FaFacebookF, FaLongArrowAltUp, FaPhoneAlt } from 'react-icons/fa';

interface Props {
  locale: Locale;
}
const FixedButton: React.FC<Props> = ({ locale }) => {
  return (
    <div className='flex flex-row sm:flex-col w-full sm:w-auto fixed right-0 bottom-0 bg-white shadow-md'>
      <Link
        target='_blank'
        rel='noopener noreferrer'
        href={PHONE_NUMBER[locale === 'en' ? 0 : 1].link}
        className='grid place-items-center w-12 h-12 border-b border-gray-100 hover:bg-green-500/10 mr-auto sm:mr-0'>
        <span className='block p-1 rounded bg-green-500'>
          <FaPhoneAlt className='text-white' />
        </span>
      </Link>
      <Link
        target='_blank'
        rel='noopener noreferrer'
        href={SOCIALS[0].link}
        className='grid place-items-center w-12 h-12 border-b border-gray-100 hover:bg-facebook/10'>
        <span className='block p-1 rounded bg-facebook'>
          <FaFacebookF className='text-white' />
        </span>
      </Link>
      <Link
        target='_blank'
        rel='noopener noreferrer'
        href={EMAIL.link}
        className='grid place-items-center w-12 h-12 border-b border-gray-100 hover:bg-primary/10'>
        <span className='block p-1 rounded bg-primary'>
          <FaEnvelope className='text-white' />
        </span>
      </Link>
      <button
        type='button'
        className='grid place-items-center w-12 h-12 hover:bg-primary/10 ml-auto sm:ml-0'
        onClick={() => {
          window.scrollTo({ top: 0, behavior: 'smooth' });
        }}>
        <span className='block p-1 rounded bg-white border border-primary'>
          <FaLongArrowAltUp className='text-primary' />
        </span>
      </button>
    </div>
  );
};

export default FixedButton;
