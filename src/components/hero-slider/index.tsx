'use client';
import Image from 'next/image';
import React from 'react';
import { SLIDES } from './constant';
import { useSlider } from './use-slider';

const HeroSlider: React.FC = () => {
  const [index] = useSlider(SLIDES.length);
  return (
    <div className='hero-slide relative w-full after:block after:pb-2/5'>
      {SLIDES.map((slide, idx) => (
        <Image
          key={idx}
          src={slide}
          alt={`slide ${idx}`}
          fill
          className={`transition-opacity duration-500 ${idx === index ? 'opacity-1' : 'opacity-0'}`}
          priority={index === 0}
        />
      ))}
    </div>
  );
};

export default HeroSlider;
