import { isGGPageSpeedDevice } from '@/utils/common';
import { useEffect, useState } from 'react';

export const useSlider = (total: number) => {
  const [index, setIndex] = useState<number>(0);

  useEffect(() => {
    if (isGGPageSpeedDevice()) return;

    const interval = setInterval(() => {
      setIndex((prev) => {
        return prev + 1 === total ? 0 : prev + 1;
      });
    }, 3000);
    return () => {
      clearInterval(interval);
    };
  }, [total]);

  return [index];
};
