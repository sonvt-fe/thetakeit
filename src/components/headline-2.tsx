import React, { ElementType } from 'react';

interface Props {
  title: string;
  TitleTag: ElementType;
}
const HeadlineTwo: React.FC<Props> = ({ title, TitleTag }) => {
  return (
    <TitleTag className='flex items-center gap-2 justify-center text-center font-medium text-primary text-3xl mb-3'>
      <span className='inline-block w-6 h-[2px] bg-primary/80' />
      <span>{title}</span>
      <span className='inline-block w-6 h-[2px] bg-primary/80' />
    </TitleTag>
  );
};

export default HeadlineTwo;
