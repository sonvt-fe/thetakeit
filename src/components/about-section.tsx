import photo from '@/public/images/about/tracy-duong.png';
import Image from 'next/image';
import Link from 'next/link';
import React, { ElementType } from 'react';
import { FaArrowRight } from 'react-icons/fa';
import Container from './container';

interface Props {
  data: {
    title: string;
    desc: string;
    btnLabel: string;
    link: string;
  };
  TitleTag: ElementType;
  showBtn?: boolean;
}

const AboutSection: React.FC<Props> = ({ data, TitleTag, showBtn = false }) => {
  return (
    <section className='bg-gray-100'>
      <Container className='flex items-center flex-col-reverse md:flex-row pt-2 lg:pt-0 gap-2 md:gap-0'>
        <div className='relative w-full md:w-1/2 lg:w-1/3 after:block after:pb-full'>
          <Image src={photo} alt={data.title} fill />
        </div>
        <div className='w-full md:w-2/3 pt-5 sm:p-10'>
          <p className='text-gray-900 text-sm mb-2 before:inline-block before:w-10 before:h-[2px] before:my-1 before:mr-3 before:bg-primary/80'>
            PARTNER, LEAD CONSULTANT
          </p>
          <TitleTag className='font-medium text-3xl text-primary mb-4'>{data.title}</TitleTag>
          <p className='text-gray-800'>{data.desc}</p>
          {showBtn && (
            <Link
              href={data.link}
              className='flex w-max items-center gap-2 py-3 px-4 mt-4 ml-auto md:ml-0 uppercase text-sm font-medium text-center text-white bg-primary sm:w-fit hover:bg-primary/90 focus:ring-4 focus:outline-none focus:ring-primary-300'>
              <span>{data.btnLabel}</span>
              <FaArrowRight />
            </Link>
          )}
        </div>
      </Container>
    </section>
  );
};

export default AboutSection;
