'use client';
import emailjs from '@emailjs/browser';
import React, { useCallback, useRef, useState } from 'react';
import { FaEnvelope, FaInfo, FaMapMarker, FaPhoneAlt, FaUser } from 'react-icons/fa';

type IFormValue = {
  fullName: string;
  email: string;
  phone: string;
  address: string;
  content: string;
};

const defaultFormValue: IFormValue = {
  fullName: '',
  email: '',
  phone: '',
  address: '',
  content: '',
};

interface Props {
  translate: {
    fullName: string;
    phone: string;
    address: string;
    content: string;
    submit: string;
  };
}
const FormContact: React.FC<Props> = ({ translate }) => {
  const form = useRef<HTMLFormElement>(null);

  const [formValue, setFormValue] = useState<IFormValue>(defaultFormValue);
  const [loading, setLoading] = useState<boolean>(false);

  const handleInputChange = useCallback((value: { [key: string]: string }) => {
    setFormValue((prev) => ({ ...prev, ...value }));
  }, []);

  const sendEmail = async (e: React.FormEvent) => {
    e.preventDefault();
    if (!form.current) return;

    try {
      setLoading(true);
      await emailjs.sendForm('service_ogowg2a', 'template_2ala6eg', form.current, 'O4Q6tqbF5hm44QDxO');
      setFormValue(defaultFormValue);
    } catch (error) {
      console.log(error);
    }
    setLoading(false);
  };

  return (
    <form ref={form} className='flex flex-col gap-4' onSubmit={sendEmail}>
      <div className='flex items-center gap-2'>
        <FaUser className='text-primary' />
        <input
          type='text'
          id='fullName'
          name='fullName'
          className='block py-2 w-full border-b border-gray-200 outline-none focus:border-primary'
          placeholder={translate.fullName}
          required
          value={formValue.fullName}
          onChange={(e) => handleInputChange({ fullName: e.target.value })}
        />
      </div>
      <div className='flex items-center gap-2'>
        <FaEnvelope className='text-primary' />
        <input
          type='email'
          id='email'
          name='email'
          className='block py-2 w-full border-b border-gray-200 outline-none focus:border-primary'
          placeholder='Email (*)'
          required
          value={formValue.email}
          onChange={(e) => handleInputChange({ email: e.target.value })}
        />
      </div>
      <div className='flex items-center gap-2'>
        <FaPhoneAlt className='text-primary' />
        <input
          type='text'
          id='phone'
          name='phone'
          className='block py-2 w-full border-b border-gray-200 outline-none focus:border-primary'
          placeholder={translate.phone}
          pattern='^(\+\d{1,2}\s?)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$'
          required
          value={formValue.phone}
          onChange={(e) => handleInputChange({ phone: e.target.value })}
        />
      </div>
      <div className='flex items-center gap-2'>
        <FaMapMarker className='text-primary' />
        <input
          type='text'
          id='address'
          name='address'
          className='block py-2 w-full border-b border-gray-200 outline-none focus:border-primary'
          placeholder={translate.address}
          value={formValue.address}
          onChange={(e) => handleInputChange({ address: e.target.value })}
        />
      </div>
      <div className='flex gap-2'>
        <FaInfo className='text-primary mt-3' />
        <textarea
          id='content'
          name='content'
          rows={2}
          className='block py-2 w-full border-b border-gray-200 outline-none focus:border-primary'
          placeholder={translate.content}
          value={formValue.content}
          onChange={(e) => handleInputChange({ content: e.target.value })}
        />
      </div>
      <button
        type='submit'
        disabled={loading}
        className='mx-auto py-3 px-5 uppercase text-sm font-medium text-center text-white bg-primary sm:w-fit hover:bg-primary/90 focus:ring-4 focus:outline-none focus:ring-primary-300'>
        {loading ? 'processing...' : translate.submit}
      </button>
    </form>
  );
};

export default FormContact;
