import React from 'react';

interface Props {
  className?: string;
  children: React.ReactNode;
}

const Container: React.FC<Props> = ({ className = '', children }) => {
  return <div className={`w-full max-w-screen-xl mx-auto pl-2 pr-2 xl:pl-5 xl:pr-5 ${className}`}>{children}</div>;
};

export default Container;
