import React from 'react';
import { IconType } from 'react-icons';

interface Props {
  text: string;
  icon: IconType;
}

const Headline: React.FC<Props> = ({ text, icon }) => {
  const Icon = icon;
  return (
    <div className='mt-2 mb-4 after:mt-[3px] after:block after:w-full after:h-[1px] after:bg-gradient-to-r after:from-gray-300 after:to-white'>
      <div className='flex items-center gap-2'>
        <Icon className='text-primary text-base' />
        <h2 className='font-bold uppercase a'>{text}</h2>
      </div>
    </div>
  );
};

export default Headline;
