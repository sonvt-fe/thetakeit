import { Locale } from '@/config/i18n-config';
import { ADDRESS, DOMAIN, EMAIL, PHONE_NUMBER, SOCIALS } from '@/constants/common';
import { NAV_LIST } from '@/constants/nav';
import servicesData from '@/mock/services';
import { cva } from 'class-variance-authority';
import Link from 'next/link';
import React from 'react';
import { FaAddressCard, FaCubes, FaEnvelope, FaGlobe, FaMapMarker, FaSitemap } from 'react-icons/fa';
import Container from '../container';
import { translate } from './constant';
import Headline from './headline';
interface Props {
  locale: Locale;
}
const Footer: React.FC<Props> = ({ locale }) => {
  return (
    <footer className='border-t border-gray-200 bg-gradient-to-t from-primary/10 to-white text-sm'>
      <div className='py-5'>
        <Container className='flex flex-wrap justify-between gap-4 sm:gap-10'>
          <div>
            <Headline text={translate.contact[locale]} icon={FaAddressCard} />
            <h3 className='font-bold uppercase mb-3'>{translate.companyName[locale]}</h3>
            <div className='flex flex-col gap-2'>
              <Link
                href={ADDRESS.link}
                className='block underline-offset-2 hover:underline hover:text-primary'
                target='_blank'
                rel='noopener noreferrer'>
                <FaMapMarker className='inline text-primary text-xs mr-2' />
                {ADDRESS.name}
              </Link>
              <div className='flex gap-2 sm:gap-5'>
                {PHONE_NUMBER.map((item, idx) => (
                  <Link
                    href={item.link}
                    key={idx}
                    className='block underline-offset-2 hover:underline hover:text-primary'
                    target='_blank'
                    rel='noopener noreferrer'>
                    {<item.icon className='inline text-primary text-xs mr-2' />}
                    {item.name} {item.desc}
                  </Link>
                ))}
              </div>
              <div className='flex gap-2 sm:gap-5'>
                <Link
                  href={EMAIL.link}
                  className='block underline-offset-2 hover:underline hover:text-primary'
                  target='_blank'
                  rel='noopener noreferrer'>
                  {<FaEnvelope className='inline text-primary text-xs mr-2' />}
                  {EMAIL.name}
                </Link>
                <Link href={DOMAIN.link} className='block underline-offset-2 hover:underline hover:text-primary'>
                  {<FaGlobe className='inline text-primary text-xs mr-2' />}
                  {DOMAIN.name}
                </Link>
              </div>
            </div>
          </div>
          <div className=''>
            <Headline text={translate.menu[locale]} icon={FaSitemap} />
            <div className='flex flex-col gap-2'>
              {NAV_LIST.map((item, idx) => (
                <Link href={item.link} key={idx} className='block underline-offset-2 hover:underline hover:text-primary'>
                  {item.name[locale]}
                </Link>
              ))}
            </div>
          </div>
          <div className='w-1/2 sm:w-auto'>
            <Headline text={translate.services[locale]} icon={FaCubes} />
            <div className='flex flex-col gap-2'>
              {servicesData[locale].map((item) => (
                <Link key={item.id} href={`/${locale}/services/${item.uri}`} className='block underline-offset-2 hover:underline hover:text-primary'>
                  {item.title}
                </Link>
              ))}
            </div>
          </div>
          <div>
            <Headline text={translate.social[locale]} icon={FaCubes} />
            <div className='flex gap-4 ml-auto'>
              {SOCIALS.map((item, idx) => (
                <Link
                  key={idx}
                  title={item.name}
                  href={item.link}
                  target='_blank'
                  rel='noopener noreferrer'
                  className={socialStyle({ bg: item.name as any })}>
                  {<item.icon />}
                </Link>
              ))}
            </div>
          </div>
        </Container>
      </div>
      <div className='bg-primary py-3'>
        <Container>
          <p className='text-xs text-white text-center'>Copyright © 2023 WWW.THETAKEIT.COM. All rights reserved</p>
        </Container>
      </div>
    </footer>
  );
};

export default Footer;

const socialStyle = cva(['block p-2 rounded-full text-sm border text-white hover:opacity-80'], {
  variants: {
    bg: {
      Facebook: 'bg-facebook',
      Twitter: 'bg-twitter',
      Linkedin: 'bg-linkedin',
      Youtube: 'bg-youtube',
    },
  },
});
