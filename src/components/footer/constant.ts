export const translate = {
  contact: {
    en: 'CONTACT US',
    vi: 'Liên hệ',
  },
  menu: {
    en: 'Menu',
    vi: 'Danh mục',
  },
  services: {
    en: 'Services',
    vi: 'Dịch vụ',
  },
  social: {
    en: 'Socials',
    vi: 'Mạng xã hội',
  },
  companyName: {
    en: 'THE TAKE IT IMC',
    vi: 'THE TAKE IT IMC',
  },
};
