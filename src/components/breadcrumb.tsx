import Link from 'next/link';
import React from 'react';
import { FaChevronRight, FaHome } from 'react-icons/fa';

interface Props {
  data: {
    name: string;
    link: string;
  }[];
}
const Breadcrumb: React.FC<Props> = ({ data }) => {
  return (
    <ul className='flex item-center overflow-auto py-4 gap-1'>
      {data.map((item, idx) => (
        <li key={idx} className='flex items-center gap-1 min-w-max text-sm text-gray-700 hover:text-primary transition-colors'>
          {idx === 0 ? <FaHome /> : <FaChevronRight className='text-xs text-gray-400' />}
          <Link href={item.link}>{item.name}</Link>
        </li>
      ))}
    </ul>
  );
};

export default Breadcrumb;
