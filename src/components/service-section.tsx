import { Locale } from '@/config/i18n-config';
import { IService } from '@/interfaces/service';
import Image from 'next/image';
import Link from 'next/link';
import React, { ElementType } from 'react';
import Container from './container';
import HeadlineTwo from './headline-2';

interface Props {
  data: IService[];
  desc: string;
  locale: Locale;
  TitleTag: ElementType;
}

const ServiceSection: React.FC<Props> = ({ desc, data, locale, TitleTag }) => {
  return (
    <section className='py-8'>
      <Container>
        <HeadlineTwo title='Services' TitleTag={TitleTag} />
        <p className='text-center mx-auto mb-8 text-gray-600 max-w-2xl'>{desc}</p>
        <div className='grid sm:grid-cols-2 lg:grid-cols-3 gap-8'>
          {data.map((item) => (
            <Link href={`/${locale}/services/${item.uri}`} key={item.id} className='group'>
              <div className='overflow-hidden relative bg-gray-200 after:block after:pb-3/4'>
                <Image src={item.photo} alt={item.title} fill className='group-hover:scale-105 transition-transform duration-500' />
              </div>
              <h3 className='mt-3 font-medium text-xl text-center uppercase group-hover:text-primary transition-colors duration-500'>{item.title}</h3>
              <p className='flex items-center justify-center gap-1 my-2'>
                <span className='block w-3 h-[2px] bg-primary' />
                <span className='block w-3 h-[2px] bg-gray-400' />
              </p>
              <p className='text-gray-600 line-clamp-3'>{item.desc}</p>
            </Link>
          ))}
        </div>
      </Container>
    </section>
  );
};

export default ServiceSection;
