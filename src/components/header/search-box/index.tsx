'use client';
import { Locale } from '@/config/i18n-config';
import { IService } from '@/interfaces/service';
import data from '@/mock/services';
import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import { FaSearch } from 'react-icons/fa';
import './index.css';

interface Props {
  locale: Locale;
}
const SearchBox: React.FC<Props> = ({ locale }) => {
  const [value, setValue] = useState<string>('');
  const [isFocus, setIsFocus] = useState<boolean>(false);
  const [list, setList] = useState<IService[]>([]);

  useEffect(() => {
    if (value === '') {
      setList([]);
      return;
    }
    const timeout = setTimeout(() => {
      setIsFocus(true);
      setList(data[locale].filter((item) => item.title.toLowerCase().indexOf(value.toLowerCase()) > -1));
    }, 500);
    return () => {
      clearTimeout(timeout);
    };
  }, [value, locale]);

  return (
    <div className='relative form-search hidden md:block'>
      <form className='h-full'>
        <input
          placeholder={locale === 'en' ? 'Search...' : 'Tìm kiếm...'}
          value={value}
          onChange={(e) => setValue(e.target.value)}
          onFocus={() => setIsFocus(true)}
          onBlur={() => {
            setTimeout(() => {
              setIsFocus(false);
            }, 500);
          }}
        />
        <button type='submit' disabled>
          <FaSearch />
        </button>
      </form>
      {isFocus && (
        <ul className='absolute top-full left-0 w-full bg-white'>
          {list.map((item) => (
            <li key={item.id} className='border-b border-gray-200'>
              <Link href={`/${locale}/services/${item.uri}`} className='block py-2 px-4 hover:bg-primary/10 hover:text-primary'>
                {item.title}
              </Link>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default SearchBox;
