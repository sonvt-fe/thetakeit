import React from 'react';
import { FaBars } from 'react-icons/fa';
import { MdOutlineClose } from 'react-icons/md';
interface Props {
  isActive: boolean;
  toggle: () => void;
}

const ToggleNav: React.FC<Props> = ({ isActive, toggle }) => {
  return (
    <button type='button' onClick={toggle} className='flex lg:hidden gap-2 items-center rounded border border-primary/30 text-primary p-2 ml-2'>
      <span className='text-sm '>MENU</span>
      {isActive ? <MdOutlineClose /> : <FaBars />}
    </button>
  );
};

export default ToggleNav;
