'use client';
import { Locale } from '@/config/i18n-config';
import { NAV_LIST } from '@/constants/nav';
import { cva } from 'class-variance-authority';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import LocaleSwitcher from './locale-switcher';
import NavLink from './nav-link';
import SearchBox from './search-box';
import ToggleNav from './toggle-nav';

interface Props {
  locale: Locale;
}

const Navbar: React.FC<Props> = ({ locale }) => {
  const refContainer = useRef<HTMLDivElement>(null);
  const [active, setActive] = useState(false);

  const toggle = useCallback(() => {
    setActive((prev) => !prev);
  }, []);

  useEffect(() => {
    const handleClick = (e: any) => {
      const { current: container } = refContainer;
      if (container && !container.contains(e.target)) {
        setActive(false);
      }
    };
    window.addEventListener('click', handleClick);
    return () => {
      window.removeEventListener('click', handleClick);
    };
  }, [refContainer]);

  return (
    <div ref={refContainer} className='flex lg:flex-1 items-center'>
      <nav className={style({ active })}>
        {NAV_LIST.map((item, idx) => (
          <NavLink href={`${item.link}`} key={idx} lang={locale} onClick={toggle}>
            {item.name[locale]}
          </NavLink>
        ))}
      </nav>
      <SearchBox locale={locale} />
      <LocaleSwitcher locale={locale} />
      <ToggleNav isActive={active} toggle={toggle} />
    </div>
  );
};

export default Navbar;

const style = cva(
  'absolute top-full lg:translate-y-0 lg:top-auto right-2 lg:right-auto lg:static flex flex-col lg:flex-row w-[16rem] rounded lg:w-auto lg:items-center lg:gap-8 lg:mr-auto bg-white shadow-sm lg:shadow-none border lg:border-none border-gray-100 transition-all',
  {
    variants: {
      active: {
        true: '-translate-y-1',
        false: 'translate-y-4 opacity-0 invisible lg:opacity-100 lg:visible',
      },
    },
    defaultVariants: {
      active: false,
    },
  },
);
