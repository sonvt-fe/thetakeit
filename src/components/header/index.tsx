import { Locale } from '@/config/i18n-config';
import { EMAIL, PHONE_NUMBER, SOCIALS } from '@/constants/common';
import logo from '@/public/logo.png';
import Image from 'next/image';
import Link from 'next/link';
import React from 'react';
import { FaEnvelope } from 'react-icons/fa';
import Container from '../container';

import HeaderBot from './header-bot';
import Navbar from './nav-bar';

interface Props {
  locale: Locale;
}

const Header: React.FC<Props> = ({ locale }) => {
  return (
    <header className='relative z-50'>
      <div className='text-white bg-primary'>
        <Container className='flex justify-between py-2'>
          <div className='flex xs:gap-4'>
            {PHONE_NUMBER.map((item, idx) => (
              <Link key={idx} href={item.link} target='_blank' rel='noopener noreferrer' className='flex items-center gap-1 hover:text-yellow-500'>
                {<item.icon />}
                <span className='text-sm'>{item.name}</span>
                <span className='hidden sm:inline text-sm'>{item.desc}</span>
              </Link>
            ))}
            <Link href={EMAIL.link} target='_blank' rel='noopener noreferrer' className='hidden md:flex items-center gap-1 hover:text-yellow-500'>
              <FaEnvelope />
              <span className='text-sm'>{EMAIL.name}</span>
            </Link>
          </div>
          <div className='hidden xs:flex gap-2 ml-auto'>
            {SOCIALS.map((item, idx) => (
              <Link
                key={idx}
                title={item.name}
                href={item.link}
                target='_blank'
                rel='noopener noreferrer'
                className={`block p-1 rounded text-xs border border-white/30 transition-colors ${item.classColor}`}>
                {<item.icon />}
              </Link>
            ))}
          </div>
        </Container>
      </div>
      <HeaderBot>
        <Container className='flex items-center h-full'>
          <div className='mr-auto lg:mr-20 max-h-full'>
            <Link href={`/${locale}`} className='main-logo block  relative w-full'>
              <Image src={logo} alt='Thetakeit' priority fill quality={100} className='object-contain' />
            </Link>
            <span className="block w-full text-gray-400 text-[9px] uppercase text-center tracking-widest">We connect the world</span>
          </div>
          <Navbar locale={locale} />
        </Container>
      </HeaderBot>
    </header>
  );
};

export default Header;
