'use client';
import { Locale } from '@/config/i18n-config';
import { cva } from 'class-variance-authority';
import Link from 'next/link';
import { useSelectedLayoutSegment } from 'next/navigation';
import React from 'react';

interface Props {
  href: string;
  lang: Locale;
  onClick: () => void;
  children: React.ReactNode;
}

const link = cva(
  [
    'relative p-4 lg:p-0 uppercase font-medium hover:text-primary transition-colors after:absolute after:bottom-0 after:left-0 after:h-[1px] after:hidden lg:after:block after:bg-primary after:transition-all border-b border-gray-100',
  ],
  {
    variants: {
      active: {
        true: 'text-primary after:w-full bg-primary/10 lg:bg-white',
        false: 'after:w-0 hover:after:w-full hover:bg-primary/10 lg:hover:bg-white',
      },
    },
    defaultVariants: {
      active: false,
    },
  },
);

const NavLink: React.FC<Props> = ({ href, lang, children, onClick }) => {
  const segment = useSelectedLayoutSegment();
  const active = href === `/${segment || ''}`;
  return (
    <Link href={`/${lang}${href}`} className={link({ active })} onClick={onClick}>
      {children}
    </Link>
  );
};

export default NavLink;
