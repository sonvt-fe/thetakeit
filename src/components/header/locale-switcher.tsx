'use client';
import { Locale } from '@/config/i18n-config';
import { cva } from 'class-variance-authority';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import React from 'react';

interface Props {
  locale: Locale;
}

const LocaleSwitcher: React.FC<Props> = ({ locale }) => {
  const pathName = usePathname();
  const redirectedPathName = (locale: string) => {
    if (!pathName) return '/';
    const segments = pathName.split('/');
    segments[1] = locale;
    return segments.join('/');
  };
  return (
    <div className='ml-2 lg:ml-8'>
      <ul className='flex'>
        {['en', 'vi'].map((item) => {
          return (
            <li key={item} className={style({ active: item === locale })}>
              <Link href={redirectedPathName(item)} replace>
                {item}
              </Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default LocaleSwitcher;

const style = cva('px-2 border-r border-gray-300 last:border-none leading-none uppercase text-sm font-medium', {
  variants: {
    active: {
      true: 'text-primary',
      false: 'text-gray-500 hover:text-black',
    },
  },
  defaultVariants: {
    active: false,
  },
});
