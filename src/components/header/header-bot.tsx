'use client';
import { cva } from 'class-variance-authority';
import React, { useEffect, useRef, useState } from 'react';

interface Props {
  children: React.ReactNode;
}
const HeaderBot: React.FC<Props> = ({ children }) => {
  const ref = useRef<HTMLDivElement>(null);
  const [fixed, setFixed] = useState<boolean>(false);
  useEffect(() => {
    const { current: el } = ref;
    if (!el) return;
    const handleScroll = (e: Event) => {
      const scrollTop = document.documentElement.scrollTop;
      setFixed(scrollTop > 38);
    };
    window.addEventListener('scroll', handleScroll, { passive: true });
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [ref]);
  return (
    <div ref={ref} className={style({ fixed })}>
      {children}
    </div>
  );
};

export default HeaderBot;

const style = cva('bg-white shadow-md transition-all duration-200', {
  variants: {
    fixed: {
      true: 'fixed top-0 left-0 z-50 w-full border-t-2 border-primary h-14',
      false: 'relative h-16',
    },
  },
  defaultVariants: {
    fixed: false,
  },
});
