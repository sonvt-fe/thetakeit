import AboutSection from '@/components/about-section';
import Container from '@/components/container';
import { Locale } from '@/config/i18n-config';
import data from '@/mock/about';
import seoData from '@/mock/seo';
import type { Metadata } from 'next';

export async function generateMetadata({ params: { lang } }: { params: { lang: Locale } }): Promise<Metadata> {
  return seoData.about[lang];
}

export default async function AboutPage({ params: { lang } }: { params: { lang: Locale } }) {
  return (
    <>
      <AboutSection data={data[lang]} TitleTag='h1' />
      <Container>
        <article
          className='prose max-w-none bg-white p-4 lg:p-12 mb-4 sm:mb-12 border border-gray-100'
          dangerouslySetInnerHTML={{ __html: data[lang].detail }}
        />
      </Container>
    </>
  );
}
