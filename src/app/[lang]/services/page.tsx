import { serviceAPI } from '@/api/service';
import ServiceSection from '@/components/service-section';
import { Locale } from '@/config/i18n-config';
import seoData from '@/mock/seo';
import type { Metadata } from 'next';

export async function generateMetadata({ params: { lang } }: { params: { lang: Locale } }): Promise<Metadata> {
  return seoData.service[lang];
}

export default async function ServicePage({ params: { lang } }: { params: { lang: Locale } }) {
  const services = await serviceAPI.getAll(lang);
  return <ServiceSection data={services} desc={seoData.service[lang].description} locale={lang} TitleTag='h1' />;
}
