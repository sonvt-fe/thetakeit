import { serviceAPI } from '@/api/service';
import Breadcrumb from '@/components/breadcrumb';
import Container from '@/components/container';
import { Locale } from '@/config/i18n-config';
import seoData from '@/mock/seo';
import { formatDate } from '@/utils/common';
import { getDictionary } from '@/utils/get-dictionary';
import type { Metadata } from 'next';
import Image from 'next/image';
import { notFound } from 'next/navigation';
import { FaCalendarAlt } from 'react-icons/fa';

export async function generateMetadata({ params: { slug, lang } }: { params: { slug: string; lang: Locale } }): Promise<Metadata> {
  const data = await serviceAPI.get(slug, lang);

  if (data) {
    return {
      title: data.title,
      description: data.desc,
    };
  }
  return seoData.home[lang];
}

export async function generateStaticParams({ params: { lang } }: { params: { lang: Locale } }) {
  const data = await serviceAPI.getAll(lang);

  return data.map((item) => ({
    slug: item.uri,
  }));
}

export default async function Page({ params: { lang, slug } }: { params: { lang: Locale; slug: string } }) {
  const data = await serviceAPI.get(slug, lang);
  const dictionary = await getDictionary(lang);

  if (!data) {
    notFound();
  }

  return (
    <Container>
      <Breadcrumb
        data={[
          {
            name: dictionary.home,
            link: `/${lang}`,
          },
          {
            name: dictionary.services,
            link: `/${lang}/services`,
          },
          {
            name: data.title,
            link: `/${lang}/services/${data.uri}`,
          },
        ]}
      />
      <h1 className='text-medium text-3xl uppercase text-primary mb-2'>{data.title}</h1>
      <p className='flex items-center gap-1 mb-6'>
        <FaCalendarAlt className='text-primary' />
        <span className='text-sm'>{formatDate(data.createAt, lang)}</span>
      </p>
      <p className='mb-6 bg-primary/5 p-3 rounded text-gray-800'>{data.desc}</p>
      <div className='mx-auto max-w-screen-sm overflow-hidden relative bg-gray-200 after:block after:pb-3/4'>
        <Image src={data.photo} alt={data.title} fill />
      </div>
      <article
        className='prose max-w-none bg-white p-4 lg:p-12 mb-4 sm:mb-12 border border-gray-100'
        dangerouslySetInnerHTML={{ __html: data.detail }}
      />
    </Container>
  );
}
