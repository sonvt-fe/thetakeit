import { serviceAPI } from '@/api/service';
import ServiceSection from '@/components/service-section';
import about from '@/mock/about';
import seoData from '@/mock/seo';
import { getDictionary } from '@/utils/get-dictionary';
import type { Metadata } from 'next';
import AboutSection from '../../components/about-section';
import HeroSlider from '../../components/hero-slider';
import { Locale } from '../../config/i18n-config';

export async function generateMetadata({ params: { lang } }: { params: { lang: Locale } }): Promise<Metadata> {
  return seoData.home[lang];
}

export default async function IndexPage({ params: { lang } }: { params: { lang: Locale } }) {
  const services = await serviceAPI.getAll(lang);
  const dictionary = await getDictionary(lang);

  return (
    <>
      <h1 className='opacity-0 invisible w-0 h-0 overflow-hidden'>{dictionary.company}</h1>
      <HeroSlider />
      <AboutSection data={about[lang]} TitleTag='h2' showBtn />
      <ServiceSection data={services} desc={seoData.service[lang].description} locale={lang} TitleTag='h2' />
    </>
  );
}
