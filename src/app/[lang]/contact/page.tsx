import Container from '@/components/container';
import FormContact from '@/components/form-contact';
import { Locale } from '@/config/i18n-config';
import { ADDRESS, DOMAIN, EMAIL, PHONE_NUMBER } from '@/constants/common';
import seoData from '@/mock/seo';
import { getDictionary } from '@/utils/get-dictionary';
import type { Metadata } from 'next';
import Link from 'next/link';
import { FaEnvelope, FaHome, FaMapMarker } from 'react-icons/fa';

export async function generateMetadata({ params: { lang } }: { params: { lang: Locale } }): Promise<Metadata> {
  return seoData.contact[lang];
}

export default async function ContactPage({ params: { lang } }: { params: { lang: Locale } }) {
  const dictionary = await getDictionary(lang);

  return (
    <Container className='pt-8 pb-10 max-w-5xl'>
      <h1 className='text-2xl sm:text-3xl text-center text-primary uppercase mb-8'>{dictionary.contact}</h1>
      <div className='flex flex-col xl:flex-row items-center justify-center gap-12'>
        <div className='w-full max-w-lg lg:w-3/5'>
          <div className='flex flex-col gap-2'>
            <h2 className='flex items-center gap-2'>
              <FaHome className='text-primary' />
              <span className='font-medium'>{dictionary.company}</span>
            </h2>
            <Link
              href={ADDRESS.link}
              target='_blank'
              rel='noopener noreferrer'
              className='flex items-center gap-2 hover:underline hover:text-primary'>
              <FaMapMarker className='text-primary' />
              <span>{ADDRESS.name}</span>
            </Link>
            <div className='flex items-center gap-2 sm:gap-5'>
              {PHONE_NUMBER.map((item, idx) => (
                <Link
                  key={idx}
                  href={ADDRESS.link}
                  target='_blank'
                  rel='noopener noreferrer'
                  className='flex items-center gap-2 hover:underline hover:text-primary'>
                  {<item.icon className='text-primary' />}
                  <span>{item.name}</span>
                </Link>
              ))}
            </div>
            <div className='flex items-center gap-2 sm:gap-5'>
              <Link
                href={EMAIL.link}
                target='_blank'
                rel='noopener noreferrer'
                className='flex items-center gap-2 hover:underline hover:text-primary'>
                <FaEnvelope className='text-primary' />
                <span>{EMAIL.name}</span>
              </Link>
              <Link href={DOMAIN.link} className='flex items-center gap-2 hover:underline hover:text-primary'>
                <FaEnvelope className='text-primary' />
                <span>{DOMAIN.name}</span>
              </Link>
            </div>
          </div>
          <iframe
            src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2887.163956283886!2d-79.42419032429035!3d43.64475705297749!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882b34fe47d327a7%3A0xe95aaac3069e8827!2s28%20Fennings%20St%2C%20Toronto%2C%20ON%20M6J%203B8%2C%20Canada!5e0!3m2!1sen!2s!4v1681922499776!5m2!1sen!2s'
            className='border-none w-full h-[225px] mt-6'
            loading='lazy'
          />
        </div>
        <div className='w-full max-w-lg lg:w-2/5'>
          <FormContact translate={dictionary.formContact} />
        </div>
      </div>
    </Container>
  );
}
