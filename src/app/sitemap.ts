import { MetadataRoute } from 'next';

export default function sitemap(): MetadataRoute.Sitemap {
  return [
    {
      url: 'https://thetakeit.com',
      lastModified: new Date(),
    },
    {
      url: 'https://thetakeit.com/about',
      lastModified: new Date(),
    },
    {
      url: 'https://thetakeit.com/services',
      lastModified: new Date(),
    },
    {
      url: 'https://thetakeit.com/services/trading-consultancy',
      lastModified: new Date(),
    },
    {
      url: 'https://thetakeit.com/services/other-services',
      lastModified: new Date(),
    },
    {
      url: 'https://thetakeit.com/contact',
      lastModified: new Date(),
    },
  ];
}