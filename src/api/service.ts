import { Locale } from '@/config/i18n-config';
import { IService } from '@/interfaces/service';
import services from '@/mock/services';
export const serviceAPI = {
  get: (slug: string, locale: Locale): Promise<IService | undefined> => {
    return new Promise((resolve) => {
      resolve(services[locale].find((item) => item.uri === slug));
    });
  },
  getAll: (locale: Locale): Promise<IService[]> => {
    return new Promise((resolve) => {
      resolve(services[locale]);
    });
  },
};
