import { FaFacebookF, FaLinkedin, FaMobileAlt, FaPhoneSquareAlt, FaTwitter } from 'react-icons/fa';

export const PHONE_NUMBER = [
  {
    name: '1-647-568-9189',
    desc: '(US & Canada)',
    link: 'tel:16475689189',
    icon: FaPhoneSquareAlt,
  },
  {
    name: '84-903-914-322',
    desc: '(VN)',
    link: 'tel:84903914322',
    icon: FaMobileAlt,
  },
];
export const EMAIL = {
  name: 'sales@thetakeit.com',
  link: 'mailto:sales@thetakeit.com',
};

export const SOCIALS = [
  {
    name: 'Facebook',
    link: 'https://www.facebook.com/duong.thithutrang.1',
    classColor: 'bg-social bg-fb',
    icon: FaFacebookF,
  },
  {
    name: 'Twitter',
    link: '#',
    classColor: 'bg-social bg-tw',
    icon: FaTwitter,
  },
  {
    name: 'Linkedin',
    link: 'https://www.linkedin.com/in/tracy-duong-cpa-cga-acca-erp-systems-5a1a131a/',
    classColor: 'bg-social bg-lki',
    icon: FaLinkedin,
  },
];

export const ADDRESS = {
  name: '28 Fennings St, Toronto, Ontario M6J3B8, Canada',
  link: 'https://goo.gl/maps/ofEUeYrEq4nBnvTe7',
};

export const DOMAIN = {
  name: 'www.thetakeit.com',
  link: '/',
};
