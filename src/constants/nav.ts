export const NAV_LIST = [
  {
    name: {
      en: 'Home',
      vi: 'Trang chủ',
    },
    link: '/',
  },
  {
    name: {
      en: 'About',
      vi: 'Giới thiệu',
    },
    link: '/about',
  },
  {
    name: {
      en: 'Services',
      vi: 'Dịch vụ',
    },
    link: '/services',
  },
  {
    name: {
      en: 'Contact',
      vi: 'Liên hệ',
    },
    link: '/contact',
  },
];
